/**
 * Color Manipulation functions
 *
 * This module provides functions for converting colors between hex and RGB formats.
 */

import { colorGenerator } from "./color_generation.js";

/**
 * Converts a color object between hex and RGB formats.
 *
 * @param {Object} color - The color object to be converted.
 * @param {string} typeConversion - The type of conversion ('hex' or 'rgb').
 * @returns {Object} The color object with converted type and code.
 */
export function convertColor(color, typeConversion) {
	if (typeConversion === 'rgb') {
		return colorGenerator.rgb(color.red, color.green, color.blue);
	}

	if (typeConversion === 'hex') {
		return colorGenerator.hex(color.red, color.green, color.blue);
	}
}
