/**
 * @file Color Flipper App
 * @description This script sets up an interactive color flipper app that generates and displays
 * random colors based on user preferences for color type (RGB or Hexadecimal).
 * It also updates the background color of the webpage and displays the generated
 * color code on the page.
 */

import { colorGenerator } from "./colors/color_generation.js";
import { convertColor } from "./colors/color_manipulation.js";
import getColorName from "./colors/color_name.js";

(() => {
	// Selecting DOM elements
	const btn = document.querySelector('[data-btnFlipper]');
	const color = document.querySelector('[data-colorCode]');
	const colorTypeSelector = document.querySelector('.nav-links');

	// Default color generator function and color object
	var generateColor = colorGenerator.rgb;
	var colorObj = {
		"type": 'rgb',
		"red": 213,
		"green": 213,
		"blue": 213,
		"code": 'rgb(213, 213, 213)'
	};

	getColorName(colorObj);

	// Event listener for color type selection
	colorTypeSelector.addEventListener("click", (event) => {
		const colorSelector = event.target.dataset.colortype;

		changeColorFormat(colorSelector);

		updateBackgroundColor(colorObj.code);
	});

	// Event listener for button click
	btn.addEventListener('click', () => {
		colorObj = generateColor();
		getColorName(colorObj);

		updateBackgroundColor(colorObj.code);
	});

	/**
	 * Changes the color format based on user selection.
	 *
	 * This function updates the color generator function and converts the color object
	 * to the selected color format (RGB or Hexadecimal).
	 *
	 * @param {string} colorFormatSelected - The color format selected by the user ('rgb' or 'hex').
	 */
	function changeColorFormat(colorFormatSelected) {
		if (colorFormatSelected === 'rgb' && colorObj.type !== 'rgb') {
			generateColor = colorGenerator.rgb;
			colorObj = convertColor(colorObj, 'rgb');
		}

		if (colorFormatSelected === 'hex' && colorObj.type !== 'hex') {
			generateColor = colorGenerator.hex;
			colorObj = convertColor(colorObj, 'hex');
		}
	}

	/**
	 * Updates the background color of the webpage and displays the generated color code.
	 *
	 * @param {string} colorCode - The color code (RGB or Hexadecimal) to set as the background color.
	 */
	function updateBackgroundColor(colorCode) {
		document.body.style.backgroundColor = colorCode;
		color.innerHTML = colorCode;
	}

})();
