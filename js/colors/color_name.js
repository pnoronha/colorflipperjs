export default async function getColorName(color) {
	const colorNameElement = document.querySelector('[data-colorName]');

	try {
		const colorName = await conectColorAPI(color);
		colorNameElement.innerHTML = colorName.name.value;
	} catch {
		colorNameElement.innerHTML = 'Unavaliable';
	}
}

async function conectColorAPI(color) {
	const apiConection = await fetch(`https://www.thecolorapi.com/id?rgb=${color.red},${color.green},${color.blue}`);

	if (!apiConection.ok)
		throw new Error();

	const apiResponse = await apiConection.json();

	return apiResponse;
}


