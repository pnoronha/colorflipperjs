/**
 * Clipboard Copy Functionality
 *
 * This script adds the functionality to copy the color code to the clipboard when a button is clicked.
 * It displays a message to indicate the success or failure of the copy operation.
 */

(() => {
	// Selecting DOM elements
	const btn = document.getElementById('btn-clipBoard'); // The button element to trigger the clipboard copy
	const colorCode = document.querySelector('[data-colorCode]'); // The element containing the color code
	const copyMessage = document.querySelector('[data-copyMessage]'); // The element to display copy messages

	/**
	 * Event handler for the clipboard copy button click.
	 * Copies the color code to the clipboard and displays a copy message.
	 */
	btn.onclick = () => {
		// Get the color code text
		const textToCopy = colorCode.textContent;

		// Attempt to copy the text to the clipboard
		const copied = copyToClipboard(textToCopy);

		// Display copy message
		copyMessage.classList.remove('hidden');
		if (copied) {
			copyMessage.innerHTML = 'Color copied';
		} else {
			copyMessage.innerHTML = 'Failed to copy';
		}

		// Hide the message after 3 seconds
		setTimeout(() => {
			copyMessage.classList.add('hidden');
		}, 3000);
	};

	/**
	 * Copies the provided text to the clipboard.
	 *
	 * @param {string} text - The text to be copied to the clipboard.
	 * @returns {boolean} A boolean indicating the success of the copy operation.
	 */
	function copyToClipboard(text) {
		const textArea = document.createElement('textarea');
		textArea.value = text;
		textArea.style.position = 'fixed';

		// Append the text area to the document body
		document.body.appendChild(textArea);

		// Focus and select the text area
		textArea.focus();
		textArea.select();

		let success = false;
		try {
			// Attempt to execute the 'copy' command
			success = document.execCommand('copy');
		} catch (err) {
			console.error('Failed to copy to clipboard:', err);
		}

		// Clean up by removing the text area from the document body
		document.body.removeChild(textArea);

		return success;
	}
})();
