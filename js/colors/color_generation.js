/**
 * Color Generation Functions
 *
 * This module provides functions for generating random colors in hexadecimal (hex) and RGB formats,
 */

/**
 * Generates a random color in hexadecimal format.
 *
 * @returns {Object} An object representing the generated color.
 * @property {string} type - The color type ('hex').
 * @property {number} red - The red channel value (0-255) of the generated color.
 * @property {number} green - The green channel value (0-255) of the generated color.
 * @property {number} blue - The blue channel value (0-255) of the generated color.
 * @property {string} code - The hexadecimal color code (#RRGGBB) of the generated color.
 */
function hex(red, green, blue) {
	// Generate random color channel values
	const redValue = red || getRandomNumber();
	const greenValue = green || getRandomNumber();
	const blueValue = blue || getRandomNumber();

	// Convert channel values to hexadecimal and create color object
	const redHexValue = decimalToHex(redValue);
	const greenHexValue = decimalToHex(greenValue);
	const blueHexValue = decimalToHex(blueValue);

	const color = {
		"type": 'hex',
		"red": redValue,
		"green": greenValue,
		"blue": blueValue,
		"code": `#${redHexValue}${greenHexValue}${blueHexValue}`
	};

	return color;
}

/**
 * Generates a random color in RGB format.
 *
 * @returns {Object} An object representing the generated color.
 * @property {string} type - The color type ('rgb').
 * @property {number} red - The red channel value (0-255) of the generated color.
 * @property {number} green - The green channel value (0-255) of the generated color.
 * @property {number} blue - The blue channel value (0-255) of the generated color.
 * @property {string} code - The RGB color code (e.g., "rgb(123, 45, 67)") of the generated color.
 */
function rgb(red, green, blue) {
	// Generate random color channel values
	const redValue = red || getRandomNumber();
	const greenValue = green || getRandomNumber();
	const blueValue = blue || getRandomNumber();

	// Create color object with RGB code
	const color = {
		type: 'rgb',
		red: redValue,
		green: greenValue,
		blue: blueValue,
		code: `rgb(${redValue}, ${greenValue}, ${blueValue})`
	};

	return color;
}

/**
 * Generates a random number between 0 and 255 (inclusive).
 *
 * @returns {number} A random integer between 0 and 255.
 */
function getRandomNumber() {
	return Math.floor(Math.random() * 256);
}

/**
 * Converts a decimal color channel value to its two-digit hexadecimal representation.
 *
 * @param {number} color - The decimal color channel value (0 to 255).
 * @returns {string} The two-digit hexadecimal representation of the color channel.
 */
function decimalToHex(color) {
	return color.toString(16).padStart(2, '0').toUpperCase();
}

/**
 * Exported object containing color generation functions.
 */
export const colorGenerator = {
	hex,
	rgb
};
